# Yoda Translator

Speak like Yoda, you will learn. Help you in time, this app will.

![Yoda](https://static.giantbomb.com/uploads/original/0/30/1465012-yoda_closeup_copy.jpg)

`` Simple Hybrid Application (Android + iOS) Built with React-Native and Redux that turns plain text into a "yoda-esque" format using the Yoda-Speak API. Built using Expo.``


## Installation

***1. cd Yoda-Translator/***


***2. npm install***


***3. exp start***


## Gives a Damn? Who does?

``` Well, basically anyone who is new to redux or expo or wants to see how react-native and redux are incorporated into an app that is not a todo-list for once (what, really?!?) can reference this project or tweak around it. Most of the implementations are basic and foundational to building a relativey well-designed and simple application.```


## Okay, I'm interested, so what could I learn?

``` You could learn the basics of react-native and redux as well as how to deal with asynchronous events in redux. Additionally, the project is designed in a scalable format such that each folder has its own components, actions and reducer though shares a common store with the alternates. Or if you're a seasoned pro, you could cultivate it to do much cooler things (like convert "yodafied" text into an alternate language and enable voice capabilities as well)```
